<?php

    $suma = function($a, $b){
        echo " {$a} + {$b} = " . ($a + $b) . "<br>";
    };

    $resta = function($a, $b){
        echo " {$a} - {$b} = " . ($a - $b) . "<br>";
    };

    $multiplicacion = function($a, $b){
        echo " {$a} x {$b} = " . ($a * $b) . "<br>";
    };

    $division = function($a, $b){
        echo " {$a} / {$b} = " . ($a / $b) . "<br>";
    };

    $suma(2, 2);
    $resta(2, 2);
    $multiplicacion(2, 2);
    $division(2, 2);

?>