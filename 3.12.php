<?php
    if($_SERVER['REQUEST_METHOD'] === 'POST'){
        extract($_POST);
        session_start();
        $_SESSION["nombre"] = $nombre;
        $_SESSION["apellido"] = $apellido;
        $_SESSION["segundoApellido"] = $segundoApellido;
        $_SESSION["email"] = $email;
        $_SESSION["telefono"] = $telefono;
        $_SESSION["data"] = $data;
        $_SESSION["direccion"] = $direccion;
        $_SESSION["poblacion"] = $poblacion;
        $_SESSION["cp"] = $cp;
        if(!empty($nombre) &&!empty($data) && !empty($email) && count($seleccionar)>=2){
            mostrar_dades_formulari($nombre, $apellido, $segundoApellido, $email, $telefono, $data, $direccion, $poblacion, $cp, $seleccionar);
        }
    
    }

    function mostrar_dades_formulari($nombre, $apellido, $segundoApellido, $email, $telefono, $data, $direccion, $poblacion, $cp, $seleccionar){
        echo "<p>";
        echo "nom: $nombre<br/>";
        echo "apellido: $apellido<br/>";
        echo "segundo apellido: $segundoApellido <br/>";
        echo "e-mail: $email<br/>";
        echo "data de naiximent: $data<br/>";
        echo "adreça: $direccion <br/>";
        echo "poblaçio $poblacion <br/>";
        echo "codi postal: $cp <br/>";
        echo "telefon: $telefono <br/>";

        
        foreach ($seleccionar as $valor){
            echo $valor."<br>";
        } 
        
    }
    
?>
<form name="input" action="<?= $_SERVER['PHP_SELF']?>" method="post">
    <label for="nombre">Nom*: </label>
    <input type="text" name="nombre" value="<?php echo htmlentities($_SESSION["nombre"]) ?>" required="required"/>
    <label for="apellido">1r Cognom*: </label>
    <input type="text" name="apellido" value="<?php echo htmlentities($_SESSION["apellido"]) ?>" required="required"/>
    <label for="segundoApellido">2n Cognom</label>
    <input type="text" name="segundoApellido" value="<?php echo htmlentities($_SESSION["segundoApellido"]) ?>"/> <br/><br/>
    <label for="email">e-mail*: </label>
    <input type="email" name="email" value="<?php echo htmlentities($_SESSION["email"]) ?>" required="required"/>
    <label for="data"> data de naiximent*: </label>
    <input type="date" name="data" value="<?php echo htmlentities($_SESSION["data"]) ?>" required="required"/> <br/><br/>
    <label for="direccion">Adreça*: </label>
    <input type="text" name="direccion" value="<?php echo htmlentities($_SESSION["direccion"]) ?>" size=40 required="required"/>
    <label for="poblacion">Població*: </label>
    <input type="text" name="poblacion" value="<?php echo htmlentities($_SESSION["poblacion"]) ?>"/>
    <label for="cp">CP*: </label> 
    <input type="text" name="cp" value="<?php echo htmlentities($_SESSION["cp"]) ?>" size=5/><br/><br/>
    <label for="telefono">Telefon: </label>
    <input type="text" name="telefono" value="<?php echo htmlentities($_SESSION["cp"]) ?>"/> <br/><br/>
    <label for="seleccionar">Mòduls matrícula (marca'n al menys 2): </label>
    <select name="seleccionar[]" multiple>
        <option value="desenrotllament&nbspweb&nbspen&nbspentorn&nbspservidor"> Dessenrotllaent web en entorn servidor </option>
        <option value="desenrotllament&nbspweb&nbspen&nbspentorn&nbspclient"> Dessenrotllament web en entorn client </option>
        <option value="disseny&nbspdinterficies&nbspweb"> Disseny d'interficies web </option>
    </select>
    <br/><br/>
    <button type="submit" name="enviar">Envia</button>
</form>